
'use strict';

global.basedir = process.env.PWD;

try {
    global.environment = require(`${basedir}/config/env.json`);
} catch(error) {
    throw new Error(`${basedir}/config/env.json is not found.`)
}

const 
    fs  = require(`fs`),
    obj = {};
    
fs.readdirSync(`${__dirname}/src`).forEach((file) => {
const 
    patern = new RegExp('.js');
    if (patern.test(file)) {
        const src = require(`${__dirname}/src/${file}`);
        obj[file.replace(/.js|.ts/g, ``)] = src;
    }
    
});

 module.exports = { ... obj };