`use strict`;

module.exports = {
    
    register :async (data) => {
        if (data.body.client.clientid) {
            control.client.updateclient(data);
            return;
        }
        //** Check If Client Code is Exist */
        let dataClientCode  = {
            clientcode : data.body.client.code
        }
        let checkClientCode = await model.client.getclientbycode(dataClientCode);
        if (checkClientCode.data.length > 0) {
            data.body = {
                            error : true,
                            message : 'Client Code Already Registered'
                        };
            amqp.publish(data.source,'client.output',data);
            return;
        }
        if (checkClientCode.error) {
            data.body = {
                            error : true,
                            message : checkClientCode.message
                        };
            amqp.publish(data.source,'client.output',data);
            return;
        }

        //** Create CLient ID */
        let id = await model.client.createid(data.body.client.code);
        if (id.error) {
            data.body = id;
            amqp.publish(data.source,'client.output',data);
            return;
        }

        //** Create CLient Information */
        data.body.client.clientid = id.data.id;
        let clientinformation = await model.client.createclientinformation(data.body);
        if (clientinformation.error) {
            data.body = clientinformation;
            amqp.publish(data.source,'user.output',data);
            return;
        }

        //** Create CLient Users */
        let clientusers = await model.client.createclientusers(data.body);
        if (clientusers.error) {
            data.body = clientusers;
            amqp.publish(data.source,'user.output',data);
            return;
        }

        //** Create Client Status */
        let dataStatus  = {
            clientid : data.body.client.clientid,
            status   : 1
        }
        let clientstatus = await model.client.createclientstatus(dataStatus);
        if (clientstatus.error) {
            data.body = clientstatus;
            amqp.publish(data.source,'user.output',data);
            return;
        }
        
        data.body = {
            success : true,
            message : 'Client created'
        }

        amqp.publish(data.source,'user.output',data);
    },

    updateclient :async (data) => {
        
        
        //** Create CLient Information */
        let deleteclientinformation = await model.client.deleteclientinformation(data.body);
        let clientinformation       = await model.client.createclientinformation(data.body);
        if (clientinformation.error) {
            data.body = clientinformation;
            amqp.publish(data.source,'user.output',data);
            return;
        }
        //** Create CLient Users */
        let deleteclientusers = await model.client.deletecreateclientusers(data.body);
        let clientusers = await model.client.createclientusers(data.body);
        if (clientusers.error) {
            data.body = clientusers;
            amqp.publish(data.source,'user.output',data);
            return;
        }
        
        data.body = {
            success : true,
            message : 'Client Updated'
        }

        amqp.publish(data.source,'user.output',data);
    },

    getrole:async (data) => {
        data.body = await model.client.getmasterrole();
        amqp.publish(data.source,'client.output',data);
    },

    getclient:async (data) => {
        data.body = await model.client.getclient(data);
        amqp.publish(data.source,'client.output',data);
    },

    getfloorclient:async (data) => {
        data.body = await model.client.getfloorclient(data);
        amqp.publish(data.source,'client.output',data);
    },

    getfloordevice:async (data) => {
        data.body = await model.client.getfloordevice(data);
        amqp.publish(data.source,'client.output',data);
    },

    createdevice:async (data) => {
        //** Validate Commucation  */
        let params          = data.body.device,
            deviceid        = params.deviceid,
            deviceidproduct = deviceid.split('_');

        if (!deviceidproduct[2]) {
            data.body = {
                error : true,
                message : `device product is not found`
            }
            amqp.publish(data.source,'client.output',data);
            return;
        }

        let product = await model.client.getproduct(deviceidproduct[2]);
        if (product.data.length == 0) {
            data.body = {
                error : true,
                message : `device product unkown`
            }
            amqp.publish(data.source,'client.output',data);
            return;
        }
        if (product.data.communication == 'lora') {
            let register = await model.lora.register({
                device : params,
                product
            })
            
            if (register.error) {
                data.body = register
                amqp.publish(data.source,'client.output',data);
                return;
            }
            let activate = await model.lora.activate({
                device : params,
                product
            })
            if (activate.error) {
                data.body = activate
                amqp.publish(data.source,'client.output',data);
                return;
            }
        }
        
        //** Create Device  */
        let DEVICE = await model.client.createdeviceid(data);
        if (DEVICE.error) {
            data.body = DEVICE;
            amqp.publish(data.source,'client.output',data);
            return;
        }
        data.body.device.id = DEVICE.data.id;

        //** Create Floor  */
        let floorid = data.body.floor.id;
        if (!floorid) {
            let FLOOR = await model.client.createfloor(data);
            if (FLOOR.error) {
                data.body = FLOOR;
                amqp.publish(data.source,'client.output',data);
                return;
            }
            data.body.floor.id = FLOOR.data.id;
        }

        //** Create Connection Between Floor and Device  */
        let FLOORDEVICE = await model.client.createfloordevice(data);
        if(FLOORDEVICE.error) {
            data.body = FLOORDEVICE;
            amqp.publish(data.source,'client.output',data);
            return;
        }

        data.body = {
            success : true,
            message : 'Success Create Device'
        }
        amqp.publish(data.source,'client.output',data);
    },
    typedevice:async (data) => {
        data.body = await model.client.gettypedevice(data);
        amqp.publish(data.source,'client.output',data);
    },
    getdevice:async (data) => {
        data.body = await model.client.getdevice(data);
        amqp.publish(data.source,'client.output',data);
    },
    getappliancefloor:async (data) => {
        //** Get List Device */
        let deviceids   = [],
            daterange   = data.params.daterange || 'hourly',
            devices     = await model.client.getfloordevice(data);

        for (let i in devices.data) {
            deviceids.push(devices.data[i].clients_device.deviceid)
        }
        data.body = {
            deviceids,
            daterange
        }

        amqp.publish(service.broker,'history.appliance',data);
    },
    getalldevice:async (data) => {
        //** Get All Clients */
        let user_id = data.headers.env.userinfo.user_id,
            devices = [];

        data.body = {
            user_id
        }
        let clients = await model.client.getclient(data);
        for (let i in clients.data) {
            data.params.clientid = clients.data[i].clientid;
            let deviceclients = await model.client.getdevice(data);
            for (let j in deviceclients.data) {
                devices.push(deviceclients.data[j])
            }
        }
        data.body ={
            success : true,
            data : devices
        }
        amqp.publish(data.source,'client.output',data);
    },
    getchangeincost:async (data) => {
        //** Get All Clients */
        let user_id     = data.headers.env.userinfo.user_id,
            daterange   = data.params.daterange || 'hourly',
            deviceids   = [],
            price       = {};

        data.body = {
            user_id
        }
        let clients = await model.client.getclient(data);
        for (let i in clients.data) {
            data.params.clientid = clients.data[i].clientid;
            let deviceclients = await model.client.getdevice(data);
            for (let j in deviceclients.data) {
                deviceids.push(deviceclients.data[j].deviceid)
            }
            price = await model.client.getdeviceprice(clients.data[i].clientid);
        }
        data.body = {
            deviceids,
            daterange,
            price : price.data
            
        }
        amqp.publish(service.broker,'history.appliance',data);
    },
    getusageperdevice:async (data) => {
        //** Get All Clients */
        let user_id     = data.headers.env.userinfo.user_id,
            daterange   = data.params.daterange || 'hourly',
            deviceids   = [],
            price       = {};

        data.body = {
            user_id
        }
        let clients = await model.client.getclient(data);
        for (let i in clients.data) {
            data.params.clientid = clients.data[i].clientid;
            let deviceclients = await model.client.getdevice(data);
            for (let j in deviceclients.data) {
                deviceids.push(deviceclients.data[j].deviceid)
            }
            price = await model.client.getdeviceprice(clients.data[i].clientid);
        }
        data.body = {
            deviceids,
            daterange,
            price : price.data

        }
        amqp.publish(service.broker,'history.usageperdevice',data);
    },
    getusageperfloor:async (data) => {
        //** Get All Clients */
        let user_id     = data.headers.env.userinfo.user_id,
            daterange   = data.params.daterange || 'hourly',
            floor       = [],
            price       = {};


        data.body = {
            user_id
        }
        let clients = await model.client.getclient(data);
        for (let i in clients.data) {
            //** Get All Floor  Client*/
            data.params.clientid = clients.data[i].clientid;
            let floorclients = await model.client.getfloorclient(data);
            
            for (let j in floorclients.data) {
                //** Get All Floor Device*/
                data.params.floorid = floorclients.data[j].id
                let device = [],
                    devices = await model.client.getfloordevice(data);

                    for (let k in devices.data) {
                        device.push(devices.data[k].clients_device.deviceid)
                    }

                floor.push({
                    id      : floorclients.data[j].id,
                    floor   : floorclients.data[j].floor,
                    desc    : floorclients.data[j].description,
                    device
                })

            }
            price = await model.client.getdeviceprice(clients.data[i].clientid);

        }
        data.body = {
            floor,
            daterange,
            price : price.data
        }
        amqp.publish(service.broker,'history.usageperfloor',data);
    },
    downloadreportdevice:async (data) =>{
        amqp.publish(service.broker,'history.exportdevice',data);
    },
    downloadreportdevice2:async (data) =>{
        let detaildevice = (deviceid) =>{
            return new Promise((resolve,reject) =>{
                db.client_device.findOne({
                    order: [
                        ['id', 'ASC']
                    ],
                   
                    where: {
                        device_id : deviceid
                    },
                    raw : true
                }).then((result) =>{
                    
                    resolve(result)
                }).catch((error) =>{
                    resolve({})
                })
            })
        }
        let devicename = data.params.device_name;
        data.params.device_detail = {};
        for (const i in devicename) {
           let devicedetail = await detaildevice(devicename[i]);
           data.params.device_detail[`${devicename[i]}`] = devicedetail;
        }
        amqp.publish(service.broker,'history.exportdevice2',data);
    },
    getdetailclientfromdevice:async (data) =>{
        let devicename = data.params.properties.devicename || null;
        if (devicename) {
            let client = await model.client.getclientbydevice(devicename);
            if (client.success) {
                amqp.publish(service.broker,'main.savedata',{
                    client : client.data,
                    device : data.params
                });
            }
        }
    },
};