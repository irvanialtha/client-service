'use strict';
const
    { time, string, json } = require(`${basedir}/app_module`)

const middleware = {

    add : (req, res, nexts) => {
        const requestid = req.requestid;

        if(!http_request[requestid]) {
            http_request[requestid] = {
                request  : req,
                response : res,
                next     : {}
            };

            req.on('timeout', () => {
                middleware.remove(requestid);
            });

            req.on('abort', () => {
                middleware.remove(requestid);
            });

            res.on('error', () => {
                middleware.remove(requestid);
            });

            res.on('finish', () => {
                middleware.remove(requestid);
            });
        }

        if(nexts) {
            for (let i in nexts) {
                http_request[requestid].next[i] = nexts[i];
            }
        }
    },

    remove : (requestid) => {
        if(http_request[requestid] !== undefined) {
            delete http_request[requestid];
        }
    }
};

module.exports = (req, res, next) => {
    req.requestid = string.random(15);

    middleware.add(req, res);
    next();

    return middleware;
};
