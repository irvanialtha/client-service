`use strict`;

module.exports = {
    get: (data)=>{
        control.client.getclient(data)
    },
    role: (data)=>{
        control.client.getrole(data)
    },
    register: (data)=>{
        control.client.register(data)
    },
    getfloorclient: (data)=>{
        control.client.getfloorclient(data)
    },
    getfloordevice: (data)=>{
        control.client.getfloordevice(data)
    },
    createdevice: (data)=>{
        control.client.createdevice(data)
    },
    typedevice: (data)=>{
        control.client.typedevice(data)
    },
    getdevice: (data)=>{
        control.client.getdevice(data)
    },
    getappliancefloor: (data)=>{
        control.client.getappliancefloor(data)
    },
    getalldevice: (data)=>{
        control.client.getalldevice(data)
    },
    getchangeincost: (data)=>{
        control.client.getchangeincost(data)
    },
    getusageperdevice: (data)=>{
        control.client.getusageperdevice(data)
    },
    getusageperfloor: (data)=>{
        control.client.getusageperfloor(data)
    },
    downloadreportdevice: (data)=>{
        control.client.downloadreportdevice(data)
    },
    downloadreportdevice2: (data)=>{
        control.client.downloadreportdevice2(data)
    },
    getdetailclientfromdevice: (data)=>{
        control.client.getdetailclientfromdevice(data)
    },
}