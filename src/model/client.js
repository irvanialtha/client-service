const client = require("../subscriber/client");

`use strict`;

module.exports = {
    createid:(clientcode)=>{
        return new Promise((resolve,reject) =>{
            db.client.create({
                client_code : clientcode,
                status_id   : 1
            })
            .then((row) => {
                resolve({
                    success : true,
                    data : row.get({ plain: true })
                })
            })
            .catch((err) => {
                console.log(err)
                resolve({
                    error : true,
                    message : 'Failed Insert Client ID'
                })
            });
        })
    },
    createclientinformation:(data) =>{
        return new Promise((resolve,reject) =>{
            db.client_information.create({
                client_id       : data.client.clientid,
                client_no       : data.client.information.clientno,
                client_name     : data.client.information.clientname
            })
            .then((row) => {
                resolve({
                    success : true,
                    data : row.get({ plain: true })
                })
            })
            .catch((err) => {
                console.log(err)
                resolve({
                    error : true,
                    message : 'Failed Insert Client Information'
                })
            });
        })
    },
    createclientusers:(data) =>{
        return new Promise((resolve,reject) =>{
            let clientid    = data.client.clientid,
                users       = data.client.users.id,
                role        = data.client.users.role,
                bulkData    = [];

            for (let i in users) {
                bulkData.push({
                    client_id   : clientid,
                    user_id     : users[i],
                    user_status : 1,
                    user_role   : role
                });
            }

            db.client_user.bulkCreate(
                bulkData
            ).then((result) => {
                let output = []
                for (let i in result) {
                    output.push(result[i].dataValues)
                    
                }
                resolve({
                    success: true,
                    data: output
                })
            }).catch(function(error) {
                console.log(error)
                resolve({
                    error : true,
                    message : 'Failed Insert Client Users'
                })
                return;
            });
        })
    },
    createclientstatus:(data) =>{
        return new Promise((resolve,reject) =>{
            db.client_status.create({
                client_id       : data.clientid,
                client_status   : data.status
            })
            .then((row) => {
                resolve({
                    success : true,
                    data : row.get({ plain: true })
                })
            })
            .catch((err) => {
                console.log(err)
                resolve({
                    error : true,
                    message : 'Failed Insert Status Client'
                })
            });
        })
    },
    getclientbycode:(data)=>{
        let output = {};
        return new Promise((resolve,reject) =>{
            db.client.findOne({
                where:{
                    client_code :{
                        [Op.eq] : data.clientcode
                    },
                    status_id:{
                        [Op.eq] : 1
                    },
                }
            }).then((rows) =>{
                if (!rows) {
                    resolve({
                        success : true,
                        data :[]
                    })
                    return;
                }
                resolve({
                    success : true,
                    data :[
                        rows.dataValues
                    ]
                })
            }).catch((error) =>{
                console.log(error)
                resolve({
                    error : true,
                    messages : 'Failed get Client Code'
                })
            })
        })
    },
    getmasterrole: (data)=>{
        return new Promise((resolve,reject) =>{
            db.master_user_role.findAll({
                order: [
                    ['id', 'ASC']
                ],
            }).then((rows) =>{
                let datalist = {
                                success : true,
                                data : []
                            }
                for (let i in rows) {
                    datalist.data.push({
                                        id          : rows[i].id,
                                        role        : rows[i].role,
                                        description : rows[i].description
                                    });
                }
                resolve(datalist)
            }).catch((error) =>{
                let datalist = {
                    success : true,
                    data : []
                }
                resolve(datalist)
            })
        })
    },

    getclient: (data)=>{
        let output = {
            success : true,
            data    : []
        },
        _where = {
            status_id : {
                [Op.eq]: 1
            }
        };
        
        if (data.body.userid) {
            _where.user_id = {
                    [Op.eq]: data.body.userid
                }
        }

        return new Promise((resolve,reject) =>{
            db.client.findAll({
                attributes : ['id','client_code'],
                include: [
                    {
                        model: db.client_information,
                        required: true,
                        attributes : ['client_no','client_name'],
                        where: {
                            status_id : {
                                [Op.eq]: 1
                            }
                        }
                    }, {
                        model: db.client_user,
                        required: true,
                        include: [
                            {
                                model: db.users_information,
                                required: false,
                                where: {
                                    status_id : {
                                        [Op.eq]: 1
                                    }
                                }
                            }
                        ],
                        where: _where
                    }
                ]
            }).then(async (rows) =>{
                if (!rows) {
                    resolve(output)
                    return;
                }
                for (let i in rows) {
                    let _data = rows[i].dataValues;
                    output.data[i] = {
                        clientid            : _data.id,
                        clientcode          : _data.client_code,
                        clientinformation   : {},
                        clientusers         : []
                    }

                    let clientinformations = _data.clients_informations
                    for (let j in clientinformations) {
                        output.data[i].clientinformation = clientinformations[j]
                    }

                    let clientusers = _data.clients_users
                    for (let k in clientusers) {
                        output.data[i].clientusers.push(clientusers[k].users_informations[0])
                    }
                }
                resolve(output)
            }).catch((error) =>{
                console.log(error)
                resolve(output)
            })
        })
    },

    getfloorclient: (data)=>{
        return new Promise((resolve,reject) =>{
            db.client_floor.findAll({
                order: [
                    ['id', 'ASC']
                ],
                where : {
                    client_id :{
                        [Op.eq] : data.params.clientid
                    }
                }
            }).then((rows) =>{
                let datalist = {
                                success : true,
                                data : []
                            }
                for (let i in rows) {
                    datalist.data.push({
                                        id          : rows[i].id,
                                        floor       : rows[i].floor,
                                        description : rows[i].description
                                    });
                }
                resolve(datalist)
            }).catch((error) =>{
                let datalist = {
                    success : true,
                    data : []
                }
                resolve(datalist)
            })
        })
    },

    getfloordevice: (data)=>{
        return new Promise((resolve,reject) =>{
            db.client_floor_device.findAll({
                order: [
                    ['id', 'ASC']
                ],
                include: [
                    {
                        model: db.client_device,
                        required: true,
                        include: [
                            {
                                model: db.master_device_type,
                                required: true
                            }
                        ],
                        where: {
                            status_id : {
                                [Op.eq]: 1
                            }
                        }
                    }
                ],
                where : {
                    client_floor_id :{
                        [Op.eq] : data.params.floorid
                    }
                }
            }).then((rows) =>{
                let datalist = {
                    success : true,
                    data : []
                }
                for (let i in rows) {
                    datalist.data.push({
                                        id            : rows[i].id, 
                                        deviceid      : rows[i].client_device_id,
                                        floorid       : rows[i].client_floor_id,
                                        clients_device: {
                                            name            : rows[i].clients_device.name,
                                            description     : rows[i].clients_device.description,
                                            type            : rows[i].clients_device.master_device_type.type,
                                            typedescription : rows[i].clients_device.master_device_type.description,
                                            deviceid        : rows[i].clients_device.device_id,
                                            devicename      : rows[i].clients_device.device_name,
                                            deviceeui       : rows[i].clients_device.device_eui,
                                        }
                                    });
                }
                resolve(datalist)
            }).catch((error) =>{
                console.log(error)
                let datalist = {
                    success : true,
                    data : []
                }
                resolve(datalist)
            })
        })
    },
    createdeviceid:(data)=>{
        let params = data.body.device;
        return new Promise((resolve,reject) =>{
            db.client_device.create({
                client_id       : params.clientid,
                name            : params.name,
                description     : params.description,
                type            : params.type,
                device_id       : params.deviceid,
                device_name     : params.devicename,
                device_eui      : params.deviceeui || null
            })
            .then((row) => {
                resolve({
                    success : true,
                    data : row.get({ plain: true })
                })
            })
            .catch((err) => {
                console.log(err)
                resolve({
                    error : true,
                    message : 'Failed Create Device '
                })
            });
        })
    },
    createfloor:(data)=>{
        let params = data.body.floor;
        return new Promise((resolve,reject) =>{
            db.client_floor.create({
                client_id       : params.clientid,
                floor           : params.floor,
                description     : params.description,
            })
            .then((row) => {
                resolve({
                    success : true,
                    data : row.get({ plain: true })
                })
            })
            .catch((err) => {
                console.log(err)
                resolve({
                    error : true,
                    message : 'Failed create floor client '
                })
            });
        })
    },
    createfloordevice:(data)=>{
        let params = data.body;
        return new Promise((resolve,reject) =>{
            db.client_floor_device.create({
                client_floor_id     : params.floor.id,
                client_device_id    : params.device.id
            })
            .then((row) => {
                resolve({
                    success : true,
                    data : row.get({ plain: true })
                })
            })
            .catch((err) => {
                console.log(err)
                resolve({
                    error : true,
                    message : 'Failed create floor device '
                })
            });
        })
    },
    gettypedevice: (data)=>{
        return new Promise((resolve,reject) =>{
            db.master_device_type.findAll({
                order: [
                    ['id', 'ASC']
                ]
            }).then((rows) =>{
                let datalist = {
                                success : true,
                                data : []
                            }
                for (let i in rows) {
                    datalist.data.push({
                                        id          : rows[i].id,
                                        type        : rows[i].type,
                                        description : rows[i].description,
                                        icon        : rows[i].icon    
                                    });
                }
                resolve(datalist)
            }).catch((error) =>{
                let datalist = {
                    success : true,
                    data : []
                }
                resolve(datalist)
            })
        })
    },
    getdevice: (data)=>{
        let clientid = data.params.clientid || 0
        return new Promise((resolve,reject) =>{
            db.client_device.findAll({
                order: [
                    ['id', 'ASC']
                ],
                include: [
                    {
                        model: db.master_device_type,
                        required: true
                    }
                ],
                where: {
                    client_id : {
                        [Op.eq]: clientid
                    }
                }
            }).then((rows) =>{
                let datalist = {
                                success : true,
                                data : []
                            }
                for (let i in rows) {
                    datalist.data.push({
                                        id          : rows[i].id,
                                        name        : rows[i].name,
                                        description : rows[i].description,
                                        type        : rows[i].master_device_type,
                                        deviceid    : rows[i].device_id,
                                        devicename  : rows[i].device_name,
                                        deviceparser: rows[i].device_parser 
                                    });
                }
                resolve(datalist)
            }).catch((error) =>{
                console.log(error)
                let datalist = {
                    success : true,
                    data : []
                }
                resolve(datalist)
            })
        })
    },
    getdeviceprice: (clientid)=>{
        let price    = 1;
        return new Promise((resolve,reject) =>{
            db.client_price.findOne({
                order: [
                    ['id', 'DESC']
                ],
                where: {
                    client_id : {
                        [Op.eq]: clientid
                    }
                }
            }).then((rows) =>{
                let datalist = {
                                success : true,
                                data : {
                                    price : rows.price || 1,
                                    currency : rows.currency || 'IDR'
                                }
                            }
                resolve(datalist)
            }).catch((error) =>{
                console.log(error)
                let datalist = {
                    success : true,
                    data :  {
                        price : 1,
                        currency : 'IDR'
                    }
                }
                resolve(datalist)
            })
        })
    },
    deleteclientinformation:(data) =>{
        return new Promise((resolve,reject) =>{
            db.client_information.update({
                status_id       : 0
            },{
                where :{
                    client_id : {
                        [Op.eq]: data.client.clientid,
                    }
                }
            })
            .then((row) => {
                resolve({
                    success : true,
                    data : row
                })
            })
            .catch((err) => {
                console.log(err)
                resolve({
                    error : true,
                    message : 'Failed Delete Client Information'
                })
            });
        })
    },
    deletecreateclientusers:(data) =>{
        return new Promise((resolve,reject) =>{
            db.client_user.update({
                status_id       : 0
            },{
                where :{
                    client_id : {
                        [Op.eq]: data.client.clientid,
                    }
                }
            })
            .then((row) => {
                resolve({
                    success : true,
                    data : row
                })
            })
            .catch((err) => {
                console.log(err)
                resolve({
                    error : true,
                    message : 'Failed Delete Client Users'
                })
            });
        })
    },
    getproduct: (product)=>{
        let output = [];
        return new Promise((resolve,reject) =>{
            db.product.findOne({
                include: [
                    {
                        model: db.product_detail,
                        required: true,
                        where: {
                            status_id : {
                                [Op.eq]: 1
                            }
                        }
                    }
                ],
                order: [
                    ['id', 'ASC']
                ],
                where:{
                    product_name :{
                        [Op.eq] : product
                    },
                    status_id :{
                        [Op.eq] : 1
                    }
                }
            }).then((row) =>{
                if (row) {
                    output = row.get({ plain: true })
                }
                resolve({
                    success : true,
                    data : output
                })
            }).catch((error) =>{
                console.log(error)
                let datalist = {
                    success : true,
                    data : []
                }
                resolve(datalist)
            })
        })
    },
    getclientbydevice: (deviceid)=>{
        let output = [];
        return new Promise((resolve,reject) =>{
            db.client.findOne({
                include: [
                    {
                        model: db.client_device,
                        required: true,
                        where: {
                            status_id : {
                                [Op.eq]: 1
                            },
                            device_id : {
                                [Op.eq]: deviceid
                            }
                        }
                    }
                ],
                order: [
                    ['id', 'ASC']
                ],
                where:{
                    status_id :{
                        [Op.eq] : 1
                    }
                }
            }).then((row) =>{
                if (row) {
                    output = row.get({ plain: true })
                }
                resolve({
                    success : true,
                    data : output
                })
            }).catch((error) =>{
                console.log(error)
                let datalist = {
                    success : true,
                    data : []
                }
                resolve(datalist)
            })
        })
    },
}