`use strict`;
const axios = require('axios'),
      url   = environment.lora.api;
      
module.exports = {
    register:(params)=>{
        return new Promise((resolve,reject) =>{
            axios({
                method: 'POST',
                url: `${url}api/devices`,
                headers : {
                    'Grpc-Metadata-Authorization' : `${environment.lora.token}`
                },
                data : {
                    device  : {
                        "applicationID": "1",
                        "description": `${params.device.description}`,
                        "devEUI": `${params.device.deviceeui}`,
                        "deviceProfileID": `${params.product.data.product_detail.device_profile_id}`,
                        "isDisabled": false,
                        "name": `${params.device.name}`,
                        "referenceAltitude": 0,
                        "skipFCntCheck": true,
                        "tags": {},
                        "variables": {}
                    }
                }
            })
            .then( (response) => {
                if (response.status == 200) {
                    resolve({
                        success : true,
                        code    : response.status,
                        message : 'success'
                    })
                } else {
                    resolve({
                        error : true,
                        code    : response.status,
                        message : `return code ${response.status} for register device` 
                    })
                }
                
            })
            .catch((error) => {
                resolve({
                    error   : true,
                    code    : error.response.status,
                    message : error.response.data.message || error.response.status
                })
            });
        })
    },
    unregister:(params)=>{
        return new Promise((resolve,reject) =>{
            axios({
                method: 'DELETE',
                url: `${url}api/devices/${params.device.deviceeui}`,
                headers : {
                    'Grpc-Metadata-Authorization' : `${environment.lora.token}`
                }
            })
            .then( (response) => {
                if (response.status == 200) {
                    resolve({
                        success : true,
                        code    : response.status,
                        message : 'success'
                    })
                } else {
                    resolve({
                        error : true,
                        code    : response.status,
                        message : `return code ${response.status} for unregister device` 
                    })
                }
                
            })
            .catch((error) => {
                resolve({
                    error   : true,
                    code    : error.response.status,
                    message : error.response.data.message || error.response.status
                })
            });
        })
    },
    activate:(params)=>{
        return new Promise((resolve,reject) =>{
            axios({
                method: 'POST',
                url: `${url}api/devices/${params.device.deviceeui}/activate`,
                headers : {
                    'Grpc-Metadata-Authorization' : `${environment.lora.token}`
                },
                data : {
                    deviceActivation: {
                        "aFCntDown"     : 0,
                        "appSKey"       : `${params.product.data.product_detail.appskey}`,
                        "devAddr"       : "11122012",
                        "devEUI"        : `${params.device.deviceeui}`,
                        "fCntUp"        : 0,
                        "fNwkSIntKey"   : `${params.product.data.product_detail.fnwksintkey}`,
                        "nFCntDown"     : 0,
                        "nwkSEncKey"    : `${params.product.data.product_detail.nwksenckey}`,
                        "sNwkSIntKey"   : `${params.product.data.product_detail.snwksintkey}`,
                      }
                }
            })
            .then( (response) => {
                if (response.status == 200) {
                    resolve({
                        success : true,
                        code    : response.status,
                        message : 'success'
                    })
                } else {
                    resolve({
                        error : true,
                        code    : response.status,
                        message : `return code ${response.status} for activate device` 
                    })
                }
                
            })
            .catch((error) => {
                resolve({
                    error   : true,
                    code    : error.response.status,
                    message : error.response.data.message || error.response.status
                })
            });
        })
    },
    deactive:(params)=>{
        return new Promise((resolve,reject) =>{
            axios({
                method: 'DELETE',
                url: `${url}api/devices/${params.device.deviceeui}/activation`,
                headers : {
                    'Grpc-Metadata-Authorization' : `${environment.lora.token}`
                }
            })
            .then( (response) => {
                if (response.status == 200) {
                    resolve({
                        success : true,
                        code    : response.status,
                        message : 'success'
                    })
                } else {
                    resolve({
                        error : true,
                        code    : response.status,
                        message : `return code ${response.status} for deactive device` 
                    })
                }
                
            })
            .catch((error) => {
                console.log(error)
                resolve({
                    error   : true,
                    code    : error.response.status,
                    message : error.response.data.message || error.response.status
                })
            });
        })
    },
    status:(params)=>{
        return new Promise((resolve,reject) =>{
            axios({
                method: 'GET',
                url: `${url}api/devices/${params.device.deviceeui}/activation`,
                headers : {
                    'Grpc-Metadata-Authorization' : `${environment.lora.token}`
                }
            })
            .then( (response) => {
                if (response.status == 200) {
                    resolve({
                        success : true,
                        code    : response.status,
                        message : 'success'
                    })
                } else {
                    resolve({
                        error : true,
                        code    : response.status,
                        message : `return code ${response.status} for deactive device` 
                    })
                }
                
            })
            .catch((error) => {
                console.log(error)
                resolve({
                    error   : true,
                    code    : error.response.status,
                    message : error.response.data.message || error.response.status
                })
            });
        })
    },
}