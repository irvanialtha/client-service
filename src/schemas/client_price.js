`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {

    return db.define(`clients_price`, {
        id        : {
            type      : Sequelize.INTEGER,
            allowNull : false,
            primaryKey: true,
            autoIncrement: true,
        },
        client_id : {
            type      : Sequelize.INTEGER,
            allowNull : false
        },
        price : {
            type      : Sequelize.FLOAT,
            allowNull : false
        },
        currency : {
            type      : Sequelize.STRING,
            allowNull : true
        },
        valid_date : {
            type      : Sequelize.DATE,
            allowNull : true
        },
        end_date : {
            type      : Sequelize.DATE,
            allowNull : true
        },
        created_at : {
            type      : Sequelize.DATE,
            allowNull : true
        },
        status_id         : {
            type      : Sequelize.STRING,
            allowNull : true
        },
    }, {
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false,
        schema          : 'client'
    });

};