`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {

    return db.define(`clients`, {
        id        : {
            type      : Sequelize.INTEGER,
            allowNull : false,
            primaryKey: true,
            autoIncrement: true,
        },
        client_code : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        created_at : {
            type      : Sequelize.DATE,
            allowNull : true
        },
        status_id         : {
            type      : Sequelize.STRING,
            allowNull : false
        },
    }, {
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false,
        schema          : 'client'
    });

};