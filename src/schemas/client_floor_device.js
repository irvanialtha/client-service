`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {

    return db.define(`clients_floor_device`, {
        id        : {
            type      : Sequelize.INTEGER,
            allowNull : false,
            primaryKey: true,
            autoIncrement: true,
        },
        client_floor_id : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        client_device_id : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        description : {
            type      : Sequelize.STRING,
            allowNull : true
        },
        created_at : {
            type      : Sequelize.DATE,
            allowNull : true
        },
        status_id         : {
            type      : Sequelize.STRING,
            allowNull : true
        },
    }, {
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false,
        schema          : 'client'
    });

};