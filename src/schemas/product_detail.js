`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {
    return db.define(`product_detail`, {
        id        : {
            type      : Sequelize.INTEGER,
            allowNull : false,
            primaryKey: true,
            autoIncrement: true,
        },
        product_id : {
            type      : Sequelize.INTEGER,
            allowNull : false
        },
        class : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        type : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        device_profile_id : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        appskey : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        fnwksintkey : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        nwksenckey : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        snwksintkey : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        created_at : {
            type      : Sequelize.DATE,
            allowNull : true
        },
        status_id         : {
            type      : Sequelize.INTEGER,
            allowNull : true
        },
    }, {
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false,
        schema          : 'client'
    });

};