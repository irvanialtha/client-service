`use strict`;

module.exports = () => {
    db.client.hasMany(db.client_information, {
        foreignKey: 'client_id'
    });
    db.client.hasMany(db.client_user, {
        foreignKey: 'client_id'
    });
    db.client.hasMany(db.client_device, {
        foreignKey: 'client_id'
    });
    db.client_user.hasMany(db.users_information, {
        sourceKey : 'user_id',
        foreignKey: 'user_id'
    });
    db.client_floor_device.hasOne(db.client_device, {
        sourceKey : 'client_device_id',
        foreignKey : 'id'
    });
    db.client_device.hasOne(db.master_device_type, {
        sourceKey : 'type',
        foreignKey : 'id'
    });
    db.product_detail.hasOne(db.product, {
        sourceKey : 'product_id',
        foreignKey : 'id'
    });
    db.product.hasOne(db.product_detail, {
        foreignKey : 'product_id'
    });
}