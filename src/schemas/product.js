`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {

    return db.define(`product`, {
        id        : {
            type      : Sequelize.INTEGER,
            allowNull : false,
            primaryKey: true,
            autoIncrement: true,
        },
        product_name : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        communication : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        create_by : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        created_at : {
            type      : Sequelize.DATE,
            allowNull : true
        },
        status_id         : {
            type      : Sequelize.INTEGER,
            allowNull : true
        },
    }, {
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false,
        schema          : 'client'
    });

};