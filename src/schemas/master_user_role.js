`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {

    return db.define(`master_user_role`, {
        id        : {
            type      : Sequelize.INTEGER,
            allowNull : false,
            primaryKey: true
        },
        role : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        description         : {
            type      : Sequelize.STRING,
            allowNull : false
        }
    }, {
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false,
        schema          : 'client'
    });

};