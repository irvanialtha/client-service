`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {

    return db.define(`clients_device`, {
        id        : {
            type      : Sequelize.INTEGER,
            allowNull : false,
            primaryKey: true,
            autoIncrement: true,
        },
        client_id : {
            type      : Sequelize.INTEGER,
            allowNull : false
        },
        name : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        description : {
            type      : Sequelize.STRING,
            allowNull : true
        },
        type : {
            type      : Sequelize.INTEGER,
            allowNull : false
        },
        device_id : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        device_name : {
            type      : Sequelize.STRING,
            allowNull : true
        },
        device_eui : {
            type      : Sequelize.STRING,
            allowNull : true
        },
        created_at : {
            type      : Sequelize.DATE,
            allowNull : true
        },
        status_id         : {
            type      : Sequelize.STRING,
            allowNull : true
        },
    }, {
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false,
        schema          : 'client'
    });

};