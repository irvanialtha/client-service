`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {

    return db.define(`clients_information`, {
        id        : {
            type      : Sequelize.INTEGER,
            allowNull : false,
            primaryKey: true,
            autoIncrement: true,
        },
        client_id : {
            type      : Sequelize.INTEGER,
            allowNull : false
        },
        client_no : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        client_name : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        created_at : {
            type      : Sequelize.DATE,
            allowNull : true
        },
        status_id         : {
            type      : Sequelize.STRING,
            allowNull : true
        },
    }, {
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false,
        schema          : 'client'
    });

};