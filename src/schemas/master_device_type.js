`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {

    return db.define(`master_device_type`, {
        id        : {
            type      : Sequelize.INTEGER,
            allowNull : false,
            primaryKey: true
        },
        type : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        description         : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        icon         : {
            type      : Sequelize.STRING,
            allowNull : true
        }
    }, {
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false,
        schema          : 'client'
    });

};