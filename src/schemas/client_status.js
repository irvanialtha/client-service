`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {

    return db.define(`clients_status`, {
        id        : {
            type      : Sequelize.INTEGER,
            allowNull : false,
            primaryKey: true,
            autoIncrement: true,
        },
        client_id : {
            type      : Sequelize.INTEGER,
            allowNull : false
        },
        client_status : {
            type      : Sequelize.INTEGER,
            allowNull : false
        },
        created_at : {
            type      : Sequelize.DATE,
            allowNull : true
        },
        status_id         : {
            type      : Sequelize.STRING,
            allowNull : true
        },
    }, {
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false,
        schema          : 'client'
    });

};